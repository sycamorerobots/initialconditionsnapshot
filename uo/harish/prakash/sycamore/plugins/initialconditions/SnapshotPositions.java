/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.util.ArrayList;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;

/**
 * @author harry
 *
 */
@PluginImplementation
public class SnapshotPositions extends InitialConditionsImpl<Point2D> {

	private static String	_author				= "Hraish Prakash";
	private static TYPE		_type				= TYPE.TYPE_2D;
	private static String	_shortDescription	= "SnapshotPositions";
	private static String	_description		= "Place the mobile robots on captured snapshot positions";
	private static String	_pluginName			= "SnapshotPositions";

	private SnapshotPositionsConfigurationPanel	_configPanel;
	private Point2D								_lastPoint;

	/**
	 * @return the configPanel
	 */
	private SnapshotPositionsConfigurationPanel getConfigPanel() {

		if (_configPanel == null) {
			_configPanel = SnapshotPositionsConfigurationPanel.getUniqueInstance();
		}
		return _configPanel;
	}

	/**
	 * @return the lastPoint
	 */
	private Point2D getLastPoint() {

		if (_lastPoint == null) {
			_lastPoint = new Point2D(0, 0);
		}
		return _lastPoint;
	}

	/**
	 * @param lastPoint
	 *            the lastPoint to set
	 */
	private void setLastPoint(Point2D lastPoint) {

		this._lastPoint = lastPoint;
	}

	public SnapshotPositions() {

	}

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		ArrayList<Point2D> selectedPositions = getConfigPanel().getSelectedPositions();
		if (selectedPositions.size() > 0) {
			setLastPoint(selectedPositions.get(robots.size() % selectedPositions.size()));
		}

		return getLastPoint();
	}

	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return getConfigPanel();
	}
}
