/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;

/**
 * @author harry
 *
 */
public class SnapshotPositionsConfigurationPanel extends SycamorePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static SnapshotPositionsConfigurationPanel uniqueInstance;

	private JScrollPane	scrollPanel;
	private JPanel		contentPanel;

	private JFileChooser	fileChooser;
	private JButton			buttonBrowse;
	private JLabel			labelBrowse;
	private JLabel			labelSelectedNumber;
	private JTextField		textBrowse;

	private ArrayList<Point2D>	robotPositions;
	private ArrayList<Point2D>	selectedPositions;

	/**
	 * @return the uniqueInstance
	 */
	public static SnapshotPositionsConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new SnapshotPositionsConfigurationPanel();
		}
		return uniqueInstance;
	}

	/**
	 * @return the scrollPanel
	 */
	private JScrollPane getScrollPanel() {

		if (scrollPanel == null) {
			scrollPanel = new JScrollPane(getContentPanel());
			scrollPanel.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

				@Override
				public void adjustmentValueChanged(AdjustmentEvent e) {

					if (e.getAdjustmentType() == AdjustmentEvent.TRACK) {
						e.getAdjustable().setBlockIncrement(170);
						e.getAdjustable().setUnitIncrement(170);
					}
				}
			});

			Dimension desiredSize = new Dimension(scrollPanel.getPreferredSize().width, 200);
			scrollPanel.setPreferredSize(desiredSize);
			scrollPanel.setMinimumSize(desiredSize);
			scrollPanel.setSize(desiredSize);
		}

		return scrollPanel;
	}

	/**
	 * @return the contentPanel
	 */
	private JPanel getContentPanel() {

		if (contentPanel == null) {

			contentPanel = new JPanel();
		}
		return contentPanel;
	}

	/**
	 * @return the fileChooser
	 */
	private JFileChooser getFileChooser() {

		if (fileChooser == null) {
			fileChooser = new JFileChooser();
		}

		return fileChooser;
	}

	/**
	 * @return the buttonBrowse
	 */
	private JButton getButtonBrowse() {

		if (buttonBrowse == null) {
			buttonBrowse = new JButton("...");
			buttonBrowse.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					getFileChooser().showOpenDialog(getContentPanel());

					if (getFileChooser().getSelectedFile() != null) {
						getTextBrowse().setText(getFileChooser().getSelectedFile().getAbsolutePath());
						try {
							processSelectedFile();
						}
						catch (ParserConfigurationException | SAXException | IOException e1) {
							JOptionPane.showOptionDialog(
								getContentPanel(),
								"The selected file is invalid",
								"Invalid File",
								JOptionPane.NO_OPTION,
								JOptionPane.ERROR_MESSAGE,
								null,
								new String[] { "OK" },
								"OK");
						}
					}
				}

			});
		}
		return buttonBrowse;
	}

	/**
	 * @return the labelBrowse
	 */
	private JLabel getLabelBrowse() {

		if (labelBrowse == null) {
			labelBrowse = new JLabel("Select Project");
		}
		return labelBrowse;
	}

	/**
	 * @return the labelSelectedNumber
	 */
	private JLabel getLabelSelectedNumber() {

		if (labelSelectedNumber == null) {
			labelSelectedNumber = new JLabel();
		}
		return labelSelectedNumber;
	}

	/**
	 * @return the textBrowse
	 */
	private JTextField getTextBrowse() {

		if (textBrowse == null) {
			textBrowse = new JTextField();
			textBrowse.setEditable(false);
		}
		return textBrowse;
	}

	/**
	 * @return the robotPositions
	 */
	private ArrayList<Point2D> getRobotPositions() {

		if (robotPositions == null) {
			robotPositions = new ArrayList<>();
		}

		return robotPositions;
	}

	/**
	 * @return the selectedPositions
	 */
	public ArrayList<Point2D> getSelectedPositions() {

		if (selectedPositions == null) {
			selectedPositions = new ArrayList<>();
		}
		return selectedPositions;
	}

	private SnapshotPositionsConfigurationPanel() {

		setupGUI();
	}

	private void setupGUI() {

		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);

		setupContentPanel();
		add(getScrollPanel());
	}

	private void setupContentPanel() {

		JPanel panel = getContentPanel();
		panel.removeAll();

		ArrayList<Point2D> robotPositions = getRobotPositions();
		getSelectedPositions().clear();

		GridBagLayout layout = new GridBagLayout();

		if (robotPositions.size() > 0) {
			layout.columnWeights = new double[] { 1, 1, 1 };
		}
		else {
			layout.columnWeights = new double[] { 0.1, 0.8, 0.1 };
		}

		layout.rowWeights = new double[robotPositions.size() + 2];

		for (int index = 0; index < layout.rowWeights.length; index++) {
			layout.rowWeights[index] = Double.MIN_VALUE;
		}

		layout.rowWeights[layout.rowWeights.length - 1] = 1;
		panel.setLayout(layout);

		GridBagConstraints c;
		int currentRow = 0;
		int currentColumn = 0;

		if (robotPositions.size() > 0) {

			for (Point2D position : robotPositions) {

				JCheckBox checkbox = new JCheckBox(String.format("(%f, %f)", position.x, position.y));
				checkbox.setSelected(true);

				checkbox.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						JCheckBox checkbox = (JCheckBox) e.getSource();
						if (checkbox.isSelected())
							getSelectedPositions().add(position);
						else
							getSelectedPositions().remove(position);

						getLabelSelectedNumber().setText(String.format("Selected: %d", getSelectedPositions().size()));
					}

				});

				getSelectedPositions().add(position);

				c = new GridBagConstraints();
				c.gridx = currentColumn;
				c.gridy = currentRow;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panel.add(checkbox, c);

				currentColumn = (currentColumn + 1) % 3;
				if (currentColumn == 0) {
					currentRow += 1;
				}
			}

			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = (currentColumn == 0 ? currentRow : ++currentRow);
			c.gridwidth = 3;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			c.insets = new Insets(20, 0, 0, 0);
			getLabelSelectedNumber().setText(String.format("Selected: %d", getSelectedPositions().size()));
			panel.add(getLabelSelectedNumber(), c);
		}

		else {
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = currentRow;
			c.fill = GridBagConstraints.BOTH;
			panel.add(getLabelBrowse(), c);

			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = currentRow;
			c.fill = GridBagConstraints.BOTH;
			panel.add(getTextBrowse(), c);

			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = currentRow;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			panel.add(getButtonBrowse(), c);

			currentRow += 1;
		}

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = currentRow;
		c.gridwidth = 2;
		panel.add(new JPanel(), c);

		revalidate();
		repaint();
	}

	private void processSelectedFile() throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(getFileChooser().getSelectedFile());

		document.getDocumentElement().normalize();

		NodeList node_robots = document.getElementsByTagName("robotsList");

		for (int listIndex = 0; listIndex < node_robots.getLength(); listIndex++) {

			NodeList node_robotList = node_robots.item(listIndex).getChildNodes();
			for (int index = 0; index < node_robotList.getLength(); index++) {
				Element element_robot = (Element) node_robotList.item(index);
				Element element_keyframe = (Element) element_robot.getElementsByTagName("keyframes").item(0).getLastChild();
				Element element_position = (Element) element_keyframe.getElementsByTagName("position").item(0);
				Point2D position = new Point2D();
				if (position.decode(element_position, TYPE.TYPE_2D)) {
					getRobotPositions().add(position);
				}
			}
		}

		setupContentPanel();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {}

	@Override
	public void updateGui() {}

	@Override
	public void reset() {}

}
